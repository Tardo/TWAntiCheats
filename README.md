# TWAntiCheats
A library for Teeworlds against perfect bots.

### How 'Install'?
**1. Launch build process of your mod. The library needs "generated" files**

**2. Download & Compile the library**
```
~$ wget -O twacv0.1.3b.tar.gz https://github.com/Tardo/TWAntiCheats/archive/v0.1.3b.tar.gz
~$ tar -zxvf twacv0.1.3b.tar.gz -C ~/
~$ cd TWAntiCheats-0.1.3b
~$ cmake -DTEEWORLDS_SRC_DIR:PATH=<your_mod_src_folder> -DCMAKE_BUILD_TYPE=Release .
~$ make && make install
```
_** Replace <your_mod_src_folder> with the full path of your mod source_

_** Installation copies automatically the "build/twac" folder into "<your_mod_src_folder>/other"_

### How Use?
**1. Edit the 'bam.lua' script of your mod**
- Add the line "Import("other/twac/twac.lua")
- Before "config:Finalize("config.lua") add "config:Add(TWAC.OptFind("TWAntiCheats", true))"
```
...

config:Add(SDL.OptFind("sdl", true))
config:Add(FreeType.OptFind("freetype", true))
config:Add(TWAC.OptFind("TWAntiCheats", true))
config:Finalize("config.lua")

...
```
- Just after "function build(settings)" add the line "config.TWAntiCheats:Apply(settings)"
```
...

function build(settings)
    -- apply twac setting
    config.TWAntiCheats:Apply(settings)
        
...
```

**2. Implement TWAC on your mod**
- Modify "CCharacter.h": Add "twac::CCharacter"...
```
...

CCharacterCore m_Core
twac::CCharacter m_AntiCheats;

...
```
_** Remember add... "#include <CCharacterAntiCheats.hpp>"_
- Modify CCharacter.cpp: In "void CCharacter::OnDirectInput(CNetObj_PlayerInput *pNewInput)", before the last line "mem_copy(&m_LatestPrevInput, &m_LatestInput, sizeof(m_LatestInput));" insert:
```
const int twac = m_AntiCheats.CheckInputs(Server()->Tick(), m_LatestInput, m_LatestPrevInput);
if (twac)
    dbg_msg("ANTI-CHEAT", "Detected! %d", twac);
```

#### Possible Results
```
 0 - NONE
 1 - FIREBOT
 2 - SPINBOT
 3 - AIMBOT
```
