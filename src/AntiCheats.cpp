/*
    BASIC ANTI-CHEATS SYSTEM FOR TEEWORLDS (SERVER SIDE)
    By unsigned char*
*/
#include "../include/AntiCheats.hpp"
#include <regex>
#include <fstream>

namespace twac
{

	const char* Funcs::TWACVersion() { return "0.1.3b"; }

	std::string Funcs::CensoreString(std::string in)
	{
		std::string result(in);
		std::ifstream wordListFile("censored_words.txt");
		if (!wordListFile.is_open())
			return result;
		std::string line;
		while (std::getline(wordListFile, line))
		{
			std::regex re(line, std::regex::icase);
			result = std::regex_replace(result, re, "****");
		}
		wordListFile.close();

		return result;
	}
}
