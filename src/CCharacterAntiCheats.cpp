/*
    BASIC ANTI-CHEATS SYSTEM FOR TEEWORLDS (SERVER SIDE)
    By unsigned char*
*/
#include "../include/CCharacterAntiCheats.hpp"

#include <base/system.h>
#include <game/gamecore.h>

#define TWAC_VERSION

namespace twac
{

	inline float cross(vec2 &a, vec2 &b)
	{
		return a.x * b.y - a.y * b.x;
	}

	//input count
	struct CInputCount
	{
		int m_Presses;
		int m_Releases;
	};

	CInputCount CountInput(int Prev, int Cur)
	{
		CInputCount c = {0, 0};
		Prev &= INPUT_STATE_MASK;
		Cur &= INPUT_STATE_MASK;
		int i = Prev;

		while(i != Cur)
		{
			i = (i+1)&INPUT_STATE_MASK;
			if(i&1)
				c.m_Presses++;
			else
				c.m_Releases++;
		}

		return c;
	}



	CCharacter::CCharacter()
	{
		m_ControlInputCount = 0;
		m_LastActionTickSpinBot = 0;
		m_ControlStatusAimBot = 0;
		m_LastNoShootTimeAimBot = 0;
		m_ControlStatusFireBot = 0;

		m_UseAntiCheats = true;
		m_UseAntiFireBot = true;
		m_UseAntiSpinBot = true;
		m_UseAntiAimBot = true;
	}

	void CCharacter::Reset()
	{
		m_ControlInputCount = 0;
		m_LastActionTickSpinBot = 0;
		m_ControlStatusAimBot = 0;
		m_LastNoShootTimeAimBot = 0;
		m_ControlStatusFireBot = 0;
	}


	int CCharacter::CheckInputs(int ServerTick, CNetObj_PlayerInput LatestInput, CNetObj_PlayerInput LatestPrevInput)
	{
		// Anti-Cheats
		if(m_UseAntiCheats && (LatestPrevInput.m_TargetX != LatestInput.m_TargetX || LatestPrevInput.m_TargetY != LatestInput.m_TargetY ||
				LatestInput.m_Fire != LatestPrevInput.m_Fire || LatestInput.m_Hook != LatestPrevInput.m_Hook))
		{
			const int fire = (CountInput(LatestPrevInput.m_Fire, LatestInput.m_Fire).m_Presses>0);
			vec2 lpi, li, nlpi, nli;
			lpi.x = LatestPrevInput.m_TargetX;
			lpi.y = LatestPrevInput.m_TargetY;
			li.x = LatestInput.m_TargetX;
			li.y = LatestInput.m_TargetY;
			nlpi = normalize(lpi);
			nli = normalize(li);


			// Anti Spin-Bot & Fire-Bot
			if (m_UseAntiSpinBot || m_UseAntiFireBot)
			{
				m_aControlInput[m_ControlInputCount].m_Cross = cross(nlpi, nli);
				m_aControlInput[m_ControlInputCount].m_Tick = ServerTick - m_LastActionTickSpinBot;
				m_aControlInput[m_ControlInputCount].m_Fire = fire; // Not used...
				++m_ControlInputCount;

				if (m_ControlInputCount == MAX_CONTROL_INPUTS)
				{
					float sumCross = 0.0f;
					float sumTime = 0.0f;
					for (int i=0; i<MAX_CONTROL_INPUTS; i++)
					{
						sumCross += m_aControlInput[i].m_Cross;
						sumTime += m_aControlInput[i].m_Tick;
					}

					float medCross = absolute(sumCross/(float)MAX_CONTROL_INPUTS);
					float medTime = absolute(sumTime/(float)MAX_CONTROL_INPUTS);

					sumCross = sumTime = 0.0f;
					for (int i=0; i<MAX_CONTROL_INPUTS; i++)
					{
						m_aControlInput[i].m_Cross -= medCross;
						m_aControlInput[i].m_Cross = m_aControlInput[i].m_Cross*m_aControlInput[i].m_Cross;
						sumCross += m_aControlInput[i].m_Cross;

						m_aControlInput[i].m_Tick -= medTime;
						m_aControlInput[i].m_Tick = m_aControlInput[i].m_Tick*m_aControlInput[i].m_Tick;
						sumTime += m_aControlInput[i].m_Tick;
					}

					float variCross = sumCross/(float)MAX_CONTROL_INPUTS;
					variCross = variCross*variCross;
					float variTime = sumTime/(float)MAX_CONTROL_INPUTS;
					variTime = variTime*variTime;

					// magical values...
					if (variTime < 0.35f && variCross > 0.0f && variCross < 0.2f)
					{
						if (medCross < 0.05f)
						{
							if (m_UseAntiFireBot && !LatestInput.m_Hook && !LatestPrevInput.m_Hook && fire)
							{
								m_ControlStatusFireBot++;
								if (m_ControlStatusFireBot > 6)
								{
									Reset();
									return FIREBOT; // Cheater Detected (Auto-Fire)
								}
							}
							else
								m_ControlStatusFireBot = 0;
						}
						if (m_UseAntiSpinBot && medTime < 1.2f)
						{
							Reset();
							return SPINBOT; // Bot Detected (Spin-Bot)
						}
					}

					m_ControlInputCount = 0;
				}

				m_LastActionTickSpinBot = ServerTick;
			}

			// Anti Aim-Bot
			if (m_UseAntiAimBot)
			{
				vec2 Dir = nli;
				const float AngleCur = GetAngle(Dir);
				Dir = nlpi;
				const float AnglePrev = GetAngle(Dir);
				const float AnglesDiff = absolute(AngleCur - AnglePrev);

				if (m_ControlStatusAimBot == 0 && AnglesDiff > 2.0f && fire)
				{
					++m_ControlStatusAimBot;
				}
				else if (m_ControlStatusAimBot > 0 && AnglesDiff > 2.0f && !fire && (time_get() - m_LastNoShootTimeAimBot) > 800000)
				{
					Reset();
					return AIMBOT; // Cheater Detected (Auto-Aim)
				}
				else if (!fire)
				{
					m_LastNoShootTimeAimBot = time_get();
					m_ControlStatusAimBot = 0;
				}
			}
		}
		//

		return 0;
	}

}
